﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Hackathon2019.Models;
using Newtonsoft.Json;
using ActionNameAttribute = System.Web.Http.ActionNameAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace Hackathon2019.Controllers
{
    public class CartaController : ApiController
    {
        private static readonly HttpClient client = new HttpClient();

        private List<Carta> Cartas()
        {
            List<Carta> result = new List<Carta>();
            for (int i = 0; i < 10; i++)
            {
                result.Add(new Carta() { Id = i + 1, Mensagem = "Mensagem " + (i + 1) });
            }

            return result;
        }

        //public HttpResponseMessage Get()
        //{
        //    var cartaList = Cartas();
        //    return Request.CreateResponse(HttpStatusCode.OK, new { Carta = cartaList });
        //}

        [HttpPut]
        [ActionName("alias_for_action")]
        public HttpResponseMessage Put(Carta carta)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent("Wrong Content!")
            };

            return response;
        }
    }
}
