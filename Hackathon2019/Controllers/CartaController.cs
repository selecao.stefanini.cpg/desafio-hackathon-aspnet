﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Hackathon2019.Models;
using ActionNameAttribute = System.Web.Http.ActionNameAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace Hackathon2019.Controllers
{
    public class CartaController : ApiController
    {
        private List<Carta> Cartas()
        {
            List<Carta> result = new List<Carta>();

            // create dummy data
            for (int i = 0; i < 10; i++)
            {
                result.Add(new Carta() { Id = i + 1, Mensagem = "Mensagem " + (i + 1) });
            }

            return result;
        }

        [HttpGet]
        [ActionName("Get_Cartas")]
        public HttpResponseMessage Get()
        {
            var cartaList = Cartas();
            return Request.CreateResponse(HttpStatusCode.OK, new { Carta = cartaList });
        }

        [HttpPut]
        [ActionName("Add_Carta")]
        public HttpResponseMessage Put(Carta carta)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent("Mensagem?")
            };

            return response;
        }
    }
}
